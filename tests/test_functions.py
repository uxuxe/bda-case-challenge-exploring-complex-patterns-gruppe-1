import sys
sys.path.append("./")

import unittest
from logging import Logger
import numpy as np
import pandas as pd
from fcmeans import FCM
from matplotlib import pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.datasets import make_blobs
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler, MinMaxScaler

from src import functions
import plotly.graph_objects as go
import matplotlib
matplotlib.use('Agg')

class TestDemo(unittest.TestCase):

    def setUp(self):
        matplotlib.use("Agg")

        self.df = pd.DataFrame({
            'A': [1, 2, np.nan, 4, 5],
            'B': [np.nan, 2, 3, 4, 5],
            'C': [1, 2, 3, np.nan, np.nan],
            'D': [1, 2, 3, 4, 5]
        })
        # abbreviation,title,min,min_text,max,max_text
        self.codebook = pd.DataFrame({
            "abbreviation": ["A", "B", "C", "D"],
            "title": ["Title A", "Title B", "Title C", "Title D"],
            "min": [2, 2, 1, 1],
            "min_text": ["Min A", "Min B", "Min C", "Min D"],
            "max": [5, 5, 3, 5],
            "max_text": ["Max A", "Max B", "Max C", "Max D"]
        })
        self.codebook.set_index("abbreviation", inplace=True)
        self.logger = functions.setup_logger("DEBUG")
        pass

    def test_logger(self):
        logger = functions.setup_logger("DEBUG")

        # check output
        self.assertIsInstance(logger, Logger, 'Logger has the wrong type.')

    def test_missing_values_overview(self):
        # Call the function with the test DataFrame
        result = functions.missing_values_overview(self.df)

        # Expected result
        expected_result = pd.DataFrame({
            'column_name': ['C', 'A', 'B'],
            'n_missing_values': [2, 1, 1]
        })

        # Assert the result is as expected
        self.assertEqual(len(result), 3)
        self.assertEqual(result.loc[result['column_name'] == 'C']["n_missing_values"].iloc[0], 2)
        self.assertEqual(result.loc[result['column_name'] == 'A']["n_missing_values"].iloc[0], 1)
        self.assertEqual(result.loc[result['column_name'] == 'B']["n_missing_values"].iloc[0], 1)

    def test_plot_histogram(self):
        # Call the function with the test DataFrame
        fig = functions.plot_histograms(self.df, self.codebook)
        self.assertIsInstance(fig, plt.Figure, 'The plot is not a matplotlib figure.')

    def test_preprocessed_data(self):
        ppd = functions.PreprocessedData(
            self.df,
            self.codebook,
            enforce_codebook=True,
            ordinalOptions={"D": functions.OrdinalOption.ONE_HOT},
            imputer=SimpleImputer(),
            transformer=StandardScaler(),
            drop_remaining_na=True,
            group_selector=lambda x: "1" if x <= "B" else "2",
            logger=self.logger,
        )

        # One-hot-encoding
        self.assertEqual(len(ppd.codebook), 8)
        self.assertEqual(ppd.codebook.loc["D_1"]["title"], "Title D (encoded level 2)")
        # Imputation + Transform+ Group_Selector
        self.assertEqual(ppd.df.loc[0, "B"], 0.0)
        # Transform + Group_Selector
        self.assertAlmostEqual(ppd.df.loc[2, "C"], 0.2635, 2)

        fig = ppd.plot_histogram()
        self.assertIsInstance(fig, plt.Figure, 'The plot is not a matplotlib figure.')

    def test_validate_data(self):
        ppd = functions.PreprocessedData(
            self.df,
            self.codebook,
            enforce_codebook=False,
            ordinalOptions={},
            imputer=None,
            transformer=None,
            drop_remaining_na=False,
            group_selector=None,
            logger=self.logger,
        )

        # Codebook validation
        self.assertEqual(ppd.validate_data(), False)  # out of range value in A
        self.assertEqual(ppd.validate_int_or_na(), True)  # out of range value in A
        missing_values_overview = ppd.missing_values_overview()
        self.assertEqual(3, len(missing_values_overview))
        self.assertEqual(4, missing_values_overview["n_missing_values"].sum())

        df2 = self.df.copy()
        df2.loc[0, "A"] = 2
        ppd2 = functions.PreprocessedData(
            df2,
            self.codebook,
            enforce_codebook=False,
            ordinalOptions={},
            imputer=None,
            transformer=None,
            drop_remaining_na=False,
            group_selector=None,
            logger=self.logger,
        )
        self.assertEqual(ppd2.validate_data(), True)  # out of range value in A

    def test_estimate_eps(self):
        # Create a fictional dataset with 2 blobs
        X, y = make_blobs(n_samples=500, center_box=(-5.0, 5.0), cluster_std=2.0, centers=2, random_state=42)

        plt.scatter(X[:, 0], X[:, 1], c=y)
        plt.show()

        df_fictional = pd.DataFrame(X, columns=['Feature1', 'Feature2'])
        eps_estimates_fictional = functions.estimate_eps(df_fictional, max_min_samples=30, plot=[2, 4, 15, 30])
        self.assertIsInstance(eps_estimates_fictional, dict, 'The result is not a dict.')
        anyVal = eps_estimates_fictional[list(eps_estimates_fictional.keys())[0]]
        self.assertIsInstance(anyVal, tuple, 'The result is not a dict of tuples.')
        self.assertEqual(2, len(anyVal), 'The tuple has the wrong length.')

    def test_calculate_distances(self):
        res = functions.calculate_distances(self.df, self.logger, n_logs=3)
        self.assertIsInstance(res, dict)
        last = -float("inf")
        for k, v in res.items():
            self.assertGreaterEqual(v, last)
            last = v

    def test_dbscan_analysis(self):
        ppdata = functions.PreprocessedData(
            self.df,
            self.codebook,
            enforce_codebook=False,
            ordinalOptions={},
            imputer=SimpleImputer(),
            transformer=None,
            drop_remaining_na=False,
            group_selector=None,
            logger=self.logger,
        )
        dbsca = functions.DbscanAnalysis(ppdata, DBSCAN(eps=2.0, min_samples=2))
        self.assertEqual(2, len(dbsca.getLabelCount()))
        self.assertIsInstance(dbsca, functions.DbscanAnalysis)
        self.assertEqual("DBSCAN", dbsca.method)
        self.assertIsInstance(dbsca.get_centroids(), pd.DataFrame)
        self.assertIsInstance(dbsca.getLabelCount(), pd.Series)
        self.assertEqual(dbsca.plotExplainedVariance(), plt)
        self.assertIsInstance(dbsca.createPCADf(), pd.DataFrame)
        self.assertEqual(dbsca.plot_scatterplots(1), None)
        self.assertEqual(dbsca.plot_histogram(), None)
        self.assertIsInstance(dbsca.get_silhouette_score(), float)
        self.assertIsInstance(dbsca.get_dunn_index(), float)
        self.assertIsInstance(dbsca.get_sse(), float)
        self.assertIsInstance(dbsca.get_sse(), float)
        self.assertIsInstance(dbsca.euclidian_distance_between_centroids(), pd.DataFrame)
        self.assertIsInstance(dbsca.average_distance_between_clusters(), pd.DataFrame)
        self.assertIsInstance(dbsca.pairwise_intra_cluster_distance(), pd.Series)
        self.assertIsInstance(dbsca.intra_cluster_centroid_distance(), pd.Series)
        self.assertIsInstance(dbsca.difference_by_centroids(-1, 0), pd.DataFrame)
        self.assertIsInstance(dbsca.difference_by_quantiles(-1, 0), pd.DataFrame)
        diff_pairwise = dbsca.difference_pairwise(-1, 0)
        self.assertIsInstance(diff_pairwise, pd.DataFrame)
        self.assertEqual(dbsca.plot_difference(diff_pairwise), None)

    def test_fcm_analysis(self):
        ppdata = functions.PreprocessedData(
            self.df,
            self.codebook,
            enforce_codebook=False,
            ordinalOptions={},
            imputer=SimpleImputer(),
            transformer=None,
            drop_remaining_na=False,
            group_selector=None,
            logger=self.logger,
        )
        fcma = functions.FCMAnalysis(ppdata, FCM(n_clusters=2, m=2.0, random_state=0))
        self.assertEqual("Fuzzy C-Means", fcma.method)
        self.assertEqual("Fuzzy C-Means", fcma.method)
        self.assertIsInstance(fcma.get_soft_sse(), float)
        self.assertEqual(fcma.plot_scatterplots(1), None)

    def test_preprocessed_data_and_provider(self):
        df2 = self.df.copy()
        df2.loc[:, "F7h"] = [1, 2, 3, 4, 5]
        codebook2 = self.codebook.copy()
        codebook2.loc["F7h"] = ["Title F7h", 1, "Min F7h", 5, "Max F7h"]
        ppdp = functions.PreprocessedDataProvider(
            df2,
            codebook2,
            logger=self.logger,
        )
        # test caching
        self.assertEqual(len(ppdp._PreprocessedDataProvider__preprocDataCache), 0)
        ppd = ppdp.get()
        self.assertIsInstance(ppd, functions.PreprocessedData)
        self.assertEqual(len(ppdp._PreprocessedDataProvider__preprocDataCache), 1)

        # test prepare_datafarmes
        all_prepared = functions.prepare_dataframes(
            ppdp,
            [functions.OrdinalOption.ONE_HOT, functions.OrdinalOption.KEEP_AS_NUMERIC],
            [SimpleImputer(), None, SimpleImputer(strategy="median")],
            [StandardScaler(), None, MinMaxScaler((0, 1)), MinMaxScaler((0, 3))],
            [None]
        )
        self.assertEqual(len(all_prepared), 24)
        # as the cache is hit once! 1 + 24 - 1
        self.assertEqual(len(ppdp._PreprocessedDataProvider__preprocDataCache), 24)

        exhaustive_df = functions.exhaustive_dbscan_clustering(
            all_prepared.head(1),
            [2],
            17,
            None,
            None
        )

        self.assertEqual(len(exhaustive_df), 17)

        # fcm scores
        res = functions.calculate_fcm_scores(ppdp.get(imputer=SimpleImputer(), ), [2, 3, 4], 2.0, 0)
        self.assertEqual(res.shape, (3, 7))

        # cohesion plot
        plot = functions.plot_cohesion(ppdp.get(imputer=SimpleImputer()), min_samples=2)
        self.assertEqual(plot, plt)

        # plot answer change scatterplot
        ppd = ppdp.get(imputer=SimpleImputer())
        t1_data = ppd.df
        t2_data = ppd.df
        fcma = functions.FCMAnalysis(ppd, FCM(n_clusters=2, m=2.0, random_state=0))
        t1_labels = fcma.labels
        t2_labels = fcma.labels
        plots = functions.plot_answer_change_scatterplot(t1_data, t1_labels, t2_data, t2_labels, ["A"], ppd.codebook, False)
        self.assertIsInstance(plots, list)
        t1_data.index.name = "unique_id"
        t2_data.index.name = "unique_id"
        self.assertIsInstance(functions.create_sankey_diagram_tchange(t1_data, t1_labels, t2_data, t2_labels), go.Figure)

    def tearDown(self):
        pass
