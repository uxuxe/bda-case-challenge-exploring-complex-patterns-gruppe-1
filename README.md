# BDA Challenge - Exploring Complex Patterns - Group 1

## Group Members: 
+ Forename: Fabian
+ Surname: Aisenbrey
+ Matriculation Number: 2308421

- Forename: Valentin
- Surname: Betz
- Matriculation Number: 2258679

+ Forename: Heinrich
+ Surname: Brauburger
+ Matriculation Number: 2212040

- Forename: Niklas
- Surname: Quendt
- Matriculation Number: 2222340

## Tasks

### Teil A (Für beide Gruppen zu DBSCAN und Fuzzy C-Means):
Beantwortet die Fragen am besten in Jupyter-Notebooks, falls möglich mit anschaulichen Visualisationen. Schreibt aber möglichst viel des Codes als Funktionen in separaten .py-Dateien.

**A.1**
Bereitet den Datensatz auf: Normalisierung, Standardisierung, one-hot Encoding bei kategorischen Variablen (F7h)
Wenn Fragen in eine ähnliche Richtung gehen, gewichtet diese thematische Gruppe, sodass sie nicht mehr Einfluss auf das Clustering hat als eine andere Gruppe.
Wie bestimmt man geeignete Parameterwerte (z. B. Anzahl der Cluster bzw. Neighborhood-Radius)?
Berechnet Cluster auf dem Datensatz jeweils für beide Methoden.
Visualisiert die Projektion der Cluster als Projektion auf die ersten zwei PCA-Komponenten und beschreibt, was ihr seht.

**A.2**
Was passiert bei verschiedenen Parameterwerten?
Wie groß sind die Cluster jeweils?
Wie kann man Entfernungen zwischen den Clustern berechnen? Welche Cluster sind näher oder weiter voneineander entfernt?
Wie kann man die Entfernungen innerhalb von Clustern berechnen? Welche Cluster sind dichter oder dünner?
Wie ist die Anzahl der Outlier/Punkte, die nicht eindeutig einem Cluster zuzuordnen sind?
Welche Messwerte sind sonst geeignet, um die Verteilung der Cluster zu beschreiben und innerhalb von Clustern zu vergleichen? Z. B. Entfernung vom Mittelpunkt, Dichte von Datenpunkten?

**A.3**
Welche Features beschreiben die Unterschiede zwischen den Clustern am besten? In welchen Dimensionen unterscheiden sie sich am meisten?
Visualisiert die Projektion der Cluster als Projektion auf die Features, in denen die Cluster sich am meisten unterscheiden.

**A.4 (Bonus)**
Wie sehen die Cluster aus, wenn man die 1-3 Features, in denen die Cluster sich am meisten unterscheiden, aus dem Datensatz weglässt?

### Teil B (Für eure Gruppe zu DBSCAN):
Jetzt kennen wir viele Methoden, Cluster zu beschreiben. In diesem Teil geht es nun um den Vergleich von Clusterings zu unterschiedlichen Zeitpunkten! Ich bekommt zwei Datensätze, die die Social Sentiments der Teilnehmenden zu zwei verschiedenen Zeitpunkten wiedergeben.
**B.1**
Vergleicht die Cluster zu den zwei verschiedenen Zeitpunkten.
Verändert sich die Anzahl der Cluster?
Welcher Anteil von Personen wechselt das Cluster, von welchem in welches? (Z. B. in Flussdiagramm zeigen)

**B.2**
Könnt ihr die Cluster aus beiden Zeitpunkten matchen? (z. B. mit einem Meta-Clustering über die Mittelpunkte oder anderem Algorithmus?)
Wie verschieben sich die Clustermittelpunkte? Stellt die Verschiebung als Vektoren dar.
Wie verändern sich die Verteilungen der Punkte in den Clustern?
Zeigt in Visualisierungen, (etwa Methoden aus A) die Veränderung der Cluster.

**B.3**
Welche Methoden sind am informativsten, um die Verschiebung der Cluster zu beschreiben?

**B.4 (Bonus)**
Findet/erfindet/verbessert einen Algorithmus, der für das Clustering von Social Sentiment-Daten geeignet ist - oder die Visualiserung der Cluster - und zeigt dessen Besonderheiten.

## Reproducibility

**IMPORTANT!**

**The notebooks need to be in trusted mode.**
Else, important plots will not be displayed.

**Operating System**: Linux (Ubuntu 24.04 LTS)

**Python Version**: 3.12

**Environment Setup**: 
````
python -m venv .venv
.venv\Scripts\activate
pip install -r requirements.txt
````

**Unittest & docstring coverage**:
````
pytest --cov-report term --cov=src tests/
docstr-coverage src -i -f
````  

**Tested script**:
The following script has been tested to create the venv and run the tests:
```bash
bash setup_and_test.bash
```

The output we have seen is found in the file `setup_and_test_output.txt`.


## Project Organization
------------
```
    ├── README.md 							<-- description of  group members, reproducibility, unittest etc.
    ├── .gitignore
    ├── data                                <-- provided datasets    
    ├── docs                                <-- provided codebook
    │
    ├── presentation                        <-- presentation files
    ├── notebooks							<-- juptyer notebooks
    ├── requirements.txt 					<-- required packages to run our submission
    ├── src
         └── functions.py                   <-- preprocessing methods of raw data
    └── tests
         └── tests.py                       <-- demo script for unittest                
	
```
## Code evaluation

To evaluate our code, run the following commands:

````
pytest --cov-report term --cov=src tests/
docstr-coverage src -i -f
````