
if [ -d ".venv" ]; then
  rm -r .venv
fi

python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

pytest --cov-report term --cov=src tests/
docstr-coverage src -i -f
