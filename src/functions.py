"""
This module contains various utility functions and classes for data preprocessing and clustering analysis.

Functions include data cleaning, feature engineering, and clustering algorithms.
"""

import logging
import os
import textwrap
import typing
from enum import Enum
from typing import Optional, Callable, List, Dict

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from fcmeans import FCM
from kneed import KneeLocator
from lets_plot import *
from pandas import DataFrame
from scipy.spatial.distance import cdist
from scipy.spatial.distance import pdist, squareform
from sklearn.base import TransformerMixin
from sklearn.cluster import DBSCAN
from sklearn.decomposition import PCA
from sklearn.impute import SimpleImputer
from sklearn.impute._base import _BaseImputer
from sklearn.metrics import silhouette_score
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.neighbors import NearestNeighbors
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
import plotly.graph_objects as go


def setup_logger(name: str, level: int = logging.ERROR, fmt: typing.Optional[str] = None,
                 datefmt: typing.Optional[str] = None) -> logging.Logger:
    """
    Sets up a logger with the given name, level, and format.
    If the logger already has handlers, this function will clear and override them.

    :param name: The name of the logger.
    :param level: The logging level. Defaults to logging.ERROR.
    :param fmt: The logging format. If None, the default format of `logging` is used.
    :param datefmt: The date format. If None, the default date format of `logging` is used.
    :return: The configured logger.
    """
    # Create a logger
    logger = logging.getLogger(name)

    # Check if the logger already has handlers. If yes, delete them:
    if logger.handlers:
        logger.handlers.clear()

    # Set the level of this logger
    logger.setLevel(level)

    # Create a console handler
    handler = logging.StreamHandler()

    # Set the level of the handler
    handler.setLevel(level)

    # Create a formatter
    formatter = logging.Formatter(fmt=fmt, datefmt=datefmt, validate=True)

    # Add the formatter to the handler
    handler.setFormatter(formatter)

    # Add the handler to the logger
    logger.addHandler(handler)

    return logger


class OrdinalOption(Enum):
    """
    An enumeration of options for handling ordinal columns during preprocessing.
    """
    KEEP_AS_NUMERIC = "keep"
    ONE_HOT = "one-hot-encoding"
    DROP = "drop"


def missing_values_overview(df: pd.DataFrame, drop_nomissing: bool = True) -> pd.DataFrame:
    """
    Calculates the number of missing values in each column of a DataFrame.

    :param df: The DataFrame to calculate missing values for.
    :param drop_nomissing: If True, only return rows that actually contain missing values.
    :return: A DataFrame where the index is the column names and the values are the number of missing values in each column.
    """
    missing_values = df.isna().sum().reset_index()
    missing_values.columns = ['column_name', 'n_missing_values']
    missing_values = missing_values.sort_values(by='n_missing_values', ascending=False)

    if drop_nomissing:
        missing_values = missing_values[missing_values['n_missing_values'] > 0]

    return missing_values


def plot_histograms(df: pd.DataFrame, codebookDf: pd.DataFrame, num_columns: int = 3, num_bins: int = 30,
                    title_width: int = 50, label_width: int = 20,
                    label: typing.Optional[pd.Series] = None) -> plt.Figure:
    """
    Plots histograms for each column in a DataFrame.

    :param df: The DataFrame containing the data to be plotted.
    :param codebookDf: The DataFrame containing the codebook information. Its index must be the column names of df.
    :param num_columns: The number of columns in the grid of plots.
    :param num_bins: The number of bins in the histograms.
    :param title_width: The width of the title of each plot; if the title is longer than this, it will be wrapped.
    :param label_width: The width of the labels of the x-axis; if the labels are longer than this, they will be wrapped.
    :param label: A Series containing the labels for the x-axis. If given, the clusters will be colored.
    :return: A matplotlib figure object containing the grid of plots.
    """
    num_rows = len(df.columns) // num_columns
    if len(df.columns) % num_columns:
        num_rows += 1

    fig, axes = plt.subplots(num_rows, num_columns, figsize=(15, num_rows * 5), sharey=True)

    for i, column in enumerate(df.columns):
        ax = axes[i // num_columns, i % num_columns]
        min_value = df[column].min()
        max_value = df[column].max()
        min_text = codebookDf.loc[column, 'min_text']
        max_text = codebookDf.loc[column, 'max_text']
        question = codebookDf.loc[column, 'title']
        wrapped_title = textwrap.fill(question + f' ({column})', title_width)

        if df[column].dropna().empty:
            pass  # No histogram to show.
        elif label is not None:
            ax.hist([df.loc[label == lbl, column].dropna() for lbl in label.unique()],
                    bins=num_bins, edgecolor='black', range=[min_value, max_value], stacked=True, label=label.unique())
            ax.set_xticks([min_value, max_value])
            ax.set_xticklabels([textwrap.fill(min_text, label_width), textwrap.fill(max_text, label_width)])
            ax.legend()
        else:
            ax.hist(df[column].dropna(), bins=num_bins, edgecolor='black', range=[min_value, max_value])
            ax.set_xticks([min_value, max_value])
            ax.set_xticklabels([textwrap.fill(min_text, label_width), textwrap.fill(max_text, label_width)])
        # ax.hist(df[column].dropna(), bins=num_bins, edgecolor='black', range=[min_value, max_value])
        ax.set_title(wrapped_title)
        ax.set_xlabel('Response')
        ax.set_ylabel('Frequency')

    if len(df.columns) % num_columns:
        for idx in range(len(df.columns) % num_columns, num_columns):
            fig.delaxes(axes[num_rows - 1, idx])

    plt.tight_layout()
    return fig


class PreprocessedData:
    """
    Preprocess data for clustering.

    :param df: The raw DataFrame to be preprocessed.
    :param codebook: The codebook DataFrame used for preprocessing.
    :param enforce_codebook: A boolean that decides whether to enforce the codebook on the data.
             If True, values outside the range specified in the codebook are replaced with NaN.
    :param f7h_option: An enum that decides what to do with the 'F7h' column in the DataFrame.
    :param transformer: A sklearn Transformer (like StandardScaler or MinMaxScaler) to standardize/normalize the DataFrame.
    :param imputer: A sklearn Imputer to fill missing values in the DataFrame.
    :param group_selector: A function to select groups for normalization.
    :param logger: An optional logger to log messages during preprocessing.
    """

    def __init__(self, df: pd.DataFrame,
                 codebook: pd.DataFrame,
                 enforce_codebook: bool = True,
                 ordinalOptions: Optional[typing.Mapping[str, OrdinalOption]] = None,
                 imputer: Optional[_BaseImputer] = None,
                 transformer: Optional[TransformerMixin | Pipeline] = None,
                 drop_remaining_na: bool = True,
                 group_selector: Optional[Callable[[str], str]] = None,
                 logger: Optional[logging.Logger] = None):
        self.df = df.copy()
        self.original_df = df.copy()
        self.codebook = codebook.copy()
        self.enforce_codebook = enforce_codebook
        self.ordinalOptions = ordinalOptions or {}
        self.transformer = transformer
        self.imputer = imputer
        self.drop_remaining_na = drop_remaining_na
        self.group_selector = group_selector
        self.logger = logger
        self._preprocess()

    def _preprocess(self) -> None:
        """
        Preprocesses the data according to the provided parameters.

        :return: The preprocessed DataFrame.
        """

        # Replace unexpected values with NaN
        if self.enforce_codebook:
            self._replace_unexpected_vals(np.nan)

        # Impute missing values if an imputer is provided
        if self.imputer:
            imputed_data = self.imputer.fit_transform(self.df)
            imputed_df = pd.DataFrame(imputed_data, columns=self.df.columns, index=self.df.index)
            self.df.update(imputed_df)

        # Drop remaining NaN values if specified
        if self.drop_remaining_na:
            self.df.dropna(inplace=True)

        # Handle ordinal columns based on the provided option
        for column, option in self.ordinalOptions.items():
            if option == OrdinalOption.KEEP_AS_NUMERIC:
                pass  # do nothing, since ordinal columns are already numeric
            elif option == OrdinalOption.ONE_HOT:
                self._one_hot_encode(column)
            elif option == OrdinalOption.DROP:
                self.df.drop(column, axis=1, inplace=True)

        # Standardize/normalize the DataFrame if a standardizer is provided
        if self.transformer is not None:
            self.df[self.df.columns] = pd.DataFrame(self.transformer.fit_transform(self.df[self.df.columns]),
                                                         columns=self.df.columns, index=self.df.index)

        # Normalize groups if a group selector function is provided
        if self.group_selector is not None:
            self._normalize_groups()

    def _normalize_groups(self) -> None:
        """
        This function normalizes the groups in a DataFrame based on the group selector function provided.
        The group selector function should take a string (column name) and return a string (group name) or None.
        If the group selector function returns None for a column, that column is not modified.

        The normalization is done by dividing each column in a group by the size of the group.
        This ensures that each group as a whole has the same weight when calculating distances using p-norms.

        Note: This function modifies the input DataFrame in-place.

        :param df: The input DataFrame. Each column represents a feature, and each row represents an observation.
        :param group_selector: The function to determine the group for each column. It should take a string (column name)
            and return a string (group name) or None. If it returns None, the column is not considered part of any group and
            is not modified.
        """
        # Get the groups using the selector
        groups = self.df.columns.to_series().apply(self.group_selector)

        # Normalize the groups
        for group in groups.unique():
            if group is not None:  # Ignore columns for which the group selector returned None
                self.df[groups[groups == group].index] /= len(groups[groups == group].index)

    def _one_hot_encode(self, column: str) -> None:
        """
        Performs one-hot encoding on a specified column of a DataFrame.

        :param df: The DataFrame to perform one-hot encoding on.
        :param column: The column to perform one-hot encoding on.
        :return: The DataFrame with the specified column one-hot encoded.
        """
        # Initialize the OneHotEncoder
        categories = self.df[column].unique()
        categories.sort()
        encoder = OneHotEncoder(sparse_output=False)

        # Fit the encoder and transform the column
        encoded_columns = encoder.fit_transform(self.df[[column]])

        # Create a DataFrame from the encoded columns
        new_labels = [f'{column}_{int(i)}' for i in range(encoded_columns.shape[1])]
        encoded_df = DataFrame(encoded_columns, columns=new_labels, index=self.df.index)

        # Drop the original column from the DataFrame and replace it with the encoded columns
        self.df.drop(column, axis=1, inplace=True)
        self.df = pd.concat([self.df, encoded_df], axis=1)

        # Add appropriate codebook entries and remove the original entry
        for i, (new_label, old_level) in enumerate(zip(new_labels, categories)):
            self.codebook.loc[new_label] = self.codebook.loc[column]
            self.codebook.loc[new_label, 'title'] = f'{self.codebook.loc[column, "title"]} (encoded level {old_level})'
            self.codebook.loc[new_label, 'min'] = 0
            self.codebook.loc[new_label, 'min_text'] = "0"
            self.codebook.loc[new_label, 'max'] = 1
            self.codebook.loc[new_label, 'max_text'] = "1"

        self.codebook.drop(column, inplace=True)

    def _replace_unexpected_vals(self, replace_with: object) -> None:
        """
        Replaces values that are not in the range specified by the codebook with NaN.

        :return: The modified DataFrame.
        """
        for column in self.df.columns:
            min_value = self.codebook.loc[column, 'min']
            max_value = self.codebook.loc[column, 'max']
            self.df[column] = self.df[column].apply(lambda x: x if min_value <= x <= max_value else replace_with)

    def validate_int_or_na(self):
        """
        Validates that all values in the DataFrame are integers or NaN.
        If a non-integer value is found, a warning is logged.

        :return: True if the data is valid, False otherwise.
        """

        def isIntOrNA(value: typing.Union[int, float]) -> bool:
            """
            Checks if a value is an integer or NaN.

            :param value: The value to check.
            :return: True if the value is an integer or NaN, False otherwise.
            """
            if pd.isna(value):
                return True
            if isinstance(value, float):
                return value.is_integer()
            if isinstance(value, int):
                return True
            return False

        is_valid = True
        # Check 1: All values are integers
        for column in self.df.columns:
            for value in set(self.df[column]):
                if not isIntOrNA(value):
                    self.logger.warning(f"Value {value} in column {column} of type {type(value)} is not an integer.")
                    is_valid = False

        return is_valid

    def validate_codebook_range(self):
        """
        Validates that all values in the DataFrame are within the range specified in the codebook.
        """
        is_valid = True
        # Check 2: All values are within the range specified in the codebook
        for column in self.df.columns:
            if column in self.codebook.index:
                min_value = self.codebook.loc[column, 'min']
                max_value = self.codebook.loc[column, 'max']
                df_min = self.df[column].min()
                df_max = self.df[column].max()
                question = self.codebook.loc[column, 'title']
                if df_min is None:
                    is_valid = False
                    self.logger.warning(f"Column {column} (\"{question}\") is empty.")
                elif not (min_value <= df_min <= df_max <= max_value):
                    is_valid = False
                    self.logger.warning(
                        f"Values in column {column} (\"{question}\") are outside the range [{min_value}, {max_value}]: Got [{df_min}, {df_max}].")
            else:
                is_valid = False
                self.logger.warning(f"Column {column} not found in codebook.")
        return is_valid

    def validate_data(self) -> bool:
        """
        Validates the data in the DataFrame.
        This function checks that all values are integers or NaN and that all values are within the range specified in the codebook.
        """
        return self.validate_int_or_na() and self.validate_codebook_range()

    def plot_histogram(self, num_columns: int = 3, num_bins: int = 30, title_width: int = 50, label_width: int = 20,
                       label: typing.Optional[pd.Series] = None, use_original_df: bool = False) -> plt.Figure:
        """
        Plots histograms for each column in the DataFrame.
        Adds minima, maxima, their respective description, and the question as title, as specified in the codebook.

        :param num_columns: The number of columns in the grid of plots.
        :param num_bins: The number of bins in the histograms.
        :param title_width: The width of the title of each plot; if the title is longer than this, it will be wrapped.
        :param label_width: The width of the labels of the x-axis; if the labels are longer than this, they will be wrapped.
        :param label: A Series containing the labels for the x-axis. If given, the clusters will be colored.
        :param use_original_df: If True, use the original DataFrame for plotting. If False, use the preprocessed DataFrame.
        :return: A matplotlib figure object containing the grid of plots.
        """
        return plot_histograms(self.original_df if use_original_df else self.df, self.codebook,
                               num_columns, num_bins,
                               title_width, label_width, label)

    def missing_values_overview(self, drop_nomissing: bool = True) -> pd.DataFrame:
        """
        Calculates the number of missing values in each column of a DataFrame.

        :param drop_nomissing: If True, only return rows that actually contain missing values.
        :return: A DataFrame where the index is the column names and the values are the number of missing values in each column.
        """
        return missing_values_overview(self.df, drop_nomissing)


def estimate_eps(data, max_min_samples, plot: bool | typing.List[int] = True):
    """
    Estimate epsilon for DBSCAN given data and max_min_samples.
    """
    # Compute distances to Kth nearest neighbor
    neigh = NearestNeighbors(n_neighbors=max_min_samples)
    nbrs = neigh.fit(data)
    distances, indices = nbrs.kneighbors(data)

    elbow_points = {}

    # get all indices that should be plotted
    if plot is True:
        plotIndices = set(range(1, distances.shape[1] + 1))
    elif plot is False:
        plotIndices = set()
    elif isinstance(plot, list):
        plotIndices = set(plot) & set(range(1, distances.shape[1] + 1))
    else:
        raise ValueError("plot must be a list or boolean. Got {plot} instead.")

    if plotIndices:
        plt.figure(figsize=(10, 5))

    for i in range(1, distances.shape[1]):
        # Sort the column
        column = np.sort(distances[:, i])

        # Find the elbow point
        kneedle = KneeLocator(range(len(column)), column, S=1.0, curve='convex', direction='increasing')
        elbow_point = kneedle.knee

        # Add the elbow point to the dictionary
        elbow_points[i + 1] = (elbow_point, distances[elbow_point][i])

        # Plot the column and the elbow point
        if i + 1 in plotIndices:
            line, = plt.plot(column, label=f'Neighbor {i + 1}')
            plt.vlines(elbow_point, plt.ylim()[0], plt.ylim()[1], linestyles='dotted', colors=line.get_color())

    if plotIndices:
        plt.ylabel('Kth nearest neighbor distance')
        plt.xlabel('Points sorted by distance')
        plt.title('Kth nearest neighbor distances')
        plt.legend()
        plt.show()

    return elbow_points


def calculate_distances(df: pd.DataFrame, logger: logging.Logger | None, n_logs=20) -> Dict[float, int]:
    """
    Calculate the Euclidean distance from each point to the origin point and
    find the number of points within each unique distance.

    :param df: The input DataFrame.
    :param logger: The logger to use for logging progress.
    :param n_logs: How many times to log progress.
    :return: A dictionary where the keys are the unique distances and the values are the number of points within each distance.
    """
    # Calculate the Euclidean distance from each point to the origin point
    distances_to_origin = np.linalg.norm(df, axis=1)
    closest_to_origin_idx = np.argmin(distances_to_origin)

    # Calculate the distance matrix
    distance_matrix = cdist(df, df, metric='euclidean')

    visited_points = {closest_to_origin_idx}
    current_distance = 0.0
    num_points_by_distance = dict()

    while len(visited_points) < len(df):
        # Print status updates 20 times
        if logger is not None and len(visited_points) % (len(df) // n_logs) == 0:
            logger.info("{:.2f}%".format(len(visited_points) / len(df) * 100))
        # Get the point closest to any of the visited points
        unvisited_points = list(set(range(len(df))) - visited_points)

        # Find the minimum distance from visited points to unvisited points
        min_distances = np.min(distance_matrix[list(visited_points)][:, unvisited_points], axis=0)
        min_distance = np.min(min_distances)
        min_point_idx = unvisited_points[np.argmin(min_distances)]

        visited_points.add(min_point_idx)

        if min_distance > current_distance:
            num_points_by_distance[current_distance] = len(visited_points)
            current_distance = min_distance

    return num_points_by_distance


class ClusterAnalysis:
    """
    A class to perform cluster analysis on preprocessed data.
    It is meant for subclassing for specific clustering methods like DBSCAN or Fuzzy C-Means.
    """
    method = "ClusterAnalysis"

    def __init__(self, ppdata: PreprocessedData, labels: pd.Series):
        """
        Initializes a ClusterAnalysis object.
        """
        self.ppdata = ppdata
        self.df = ppdata.df
        self.labels = labels
        self.pca = PCA(n_components=len(ppdata.df.columns))
        self.X_pca = self.pca.fit_transform(self.df[ppdata.df.columns])
        self.__silhouette_score = None
        self.__dunn_index = None
        self.__sse = None
        self.__centroids = None

    def get_centroids(self):
        """
        Returns the cluster centroids (means) for the current clustering.
        """
        if self.__centroids is None:
            self.__centroids = self._calculate_centroids()
        return self.__centroids

    def _calculate_centroids(self):
        """
        Calculates the cluster centroids for the current clustering.
        """
        unique_labels = self.labels.unique()
        centroids = dict()

        for label in unique_labels:
            data_in_cluster = self.df[self.labels == label]
            centroid = data_in_cluster.mean(axis=0)
            centroids[label] = centroid

        return pd.DataFrame(centroids).T

    def getLabelCount(self):
        """
        Returns the number of data points in each cluster.
        """
        return pd.Series(self.labels).groupby(self.labels).count()

    def plotExplainedVariance(self):
        """
        Plots the explained variance by the number of components.
        """
        plt.figure(figsize=(10, 6))
        plt.plot(range(0, len(self.pca.explained_variance_ratio_) + 1),
                 [0.0] + list(np.cumsum(self.pca.explained_variance_ratio_)))
        plt.xlabel('Number of Components')
        plt.ylabel('Cumulative Explained Variance')
        plt.title('Explained Variance by Components')
        plt.ylim(0, 1)
        return plt

    def createPCADf(self):
        """
        Creates a DataFrame containing the PCA components for each feature.
        """
        df_pca = pd.DataFrame(
            columns=['abbreviation', 'question'] + list(
                "comp" + str(i + 1) for i in range(len(self.df.columns))))
        for feature, component in zip(self.df.columns, self.pca.components_.T):
            question = None if not feature in self.ppdata.codebook.index else self.ppdata.codebook.loc[feature, 'title']
            df_pca.loc[len(df_pca)] = [feature, question] + list(
                component[i] for i in range(len(self.df.columns)))
        return df_pca

    def plot_scatterplots(self, n_scatterplots):
        """
        Plots scatterplots of the data projected onto two principal components per plot.
        """
        # Check that the number of scatterplots is valid
        if n_scatterplots * 2 > len(self.df.columns):
            raise ValueError("n_scatterplots * 2 cannot be greater than the number of dimensions in the DataFrame")

        # Discrete color labels
        unique_labels = np.unique(self.labels)
        cmap = plt.get_cmap('viridis', len(unique_labels))
        legend_handles = [mpatches.Patch(color=cmap(i), label=f'Cluster {label}') for i, label in
                          enumerate(unique_labels)]

        # Create scatterplots
        for i in range(n_scatterplots):
            plt.figure(figsize=(16, 12))
            plt.scatter(self.X_pca[:, i * 2], self.X_pca[:, i * 2 + 1], c=self.labels, cmap='viridis', edgecolor='k')
            plt.title(f'{self.method} Clustering Projection on PCA Components {i * 2 + 1} and {i * 2 + 2}')
            plt.xlabel(f'PCA Component {i * 2 + 1}')
            plt.ylabel(f'PCA Component {i * 2 + 2}')
            plt.legend(handles=legend_handles)
            plt.show()

    def plot_histogram(self):
        """
        Plots histograms for each feature, colored by cluster.
        """
        self.ppdata.plot_histogram(label=self.labels)

    def get_silhouette_score(self):
        """
        Returns the silhouette score for the current clustering.
        """
        if self.__silhouette_score is None:
            self.__silhouette_score = self._calculate_silhouette_score()
        return self.__silhouette_score

    def get_dunn_index(self):
        """
        Returns the Dunn index for the current clustering.
        """
        if self.__dunn_index is None:
            self.__dunn_index = self._calculate_dunn_index()
        return self.__dunn_index

    def get_sse(self):
        """
        Returns the sum of squared distances (SSE) for the current clustering.
        """
        if self.__sse is None:
            self.__sse = self._calculate_sse()
        return self.__sse

    def _calculate_silhouette_score(self):
        """
        Calculates the silhouette score for the current clustering.
        """
        return 0.0 if self.labels.unique().size == 1 else silhouette_score(self.df, self.labels)

    def _calculate_dunn_index(self):
        """
        Calculates the Dunn index for the current clustering.
        """
        unique_labels = np.unique(self.labels)
        inter_cluster_distances = np.inf
        intra_cluster_distances = 0

        if len(unique_labels) == 1:
            return 0.0

        for label in unique_labels:
            data_in_cluster = self.df[self.labels == label]
            data_outside_cluster = self.df[self.labels != label]
            distances = cdist(data_in_cluster, data_outside_cluster)
            inter_cluster_distances = min(inter_cluster_distances, np.min(distances))

            distances = euclidean_distances(data_in_cluster)
            if distances.size > 0:
                intra_cluster_distances = max(intra_cluster_distances, np.max(distances))

        if intra_cluster_distances == 0:
            return 0.0

        return inter_cluster_distances / intra_cluster_distances

    def _calculate_sse(self):
        """
        Calculates the sum of squared distances (SSE) between data points and their cluster centroids (mean).
        """
        unique_labels = np.unique(self.labels)
        sse = 0.0

        for label in unique_labels:
            data_in_cluster = self.df[self.labels == label]
            centroid = data_in_cluster.mean(axis=0)
            distances = cdist(data_in_cluster, centroid.values[np.newaxis, :])
            sse += np.sum(distances ** 2)

        return sse

    def euclidian_distance_between_centroids(self):
        """
        Calculate the Euclidean distance between each pair of cluster centroids.
        """
        # Convert the centroids dictionary to a DataFrame
        distances = squareform(pdist(self.get_centroids(), metric='euclidean'))
        label_names = list(self.get_centroids().index)
        return pd.DataFrame(distances, index=label_names, columns=label_names)

    def average_distance_between_clusters(self):
        """
        Calculate the average distance between each pair of clusters.

        Returns: A DataFrame where the rows and columns are the cluster labels and the values are the average distances between the clusters.
        """
        unique_labels = self.labels.unique()

        # Initialize a matrix to store the average distances
        avg_distances = np.zeros((len(unique_labels), len(unique_labels)))

        # Calculate the average distance between each pair of clusters
        for i, label1 in enumerate(unique_labels):
            for j, label2 in enumerate(unique_labels):
                if i < j:  # Avoid calculating the distance twice for the same pair of clusters; for i=j the distance is 0.
                    cluster1 = self.df[self.labels == label1]
                    cluster2 = self.df[self.labels == label2]
                    distances = cdist(cluster1, cluster2)
                    avg_distances[i, j] = distances.mean()
                    avg_distances[j, i] = avg_distances[i, j]  # The distance is symmetric

        return pd.DataFrame(avg_distances, index=unique_labels, columns=unique_labels)

    def pairwise_intra_cluster_distance(self) -> pd.Series:
        """
        Calculates the average distance within each cluster.
        """
        unique_labels = self.labels.unique()

        # Initialize a matrix to store the average distances
        avg_distances = np.zeros([len(unique_labels)])

        # Calculate the average distance between each pair of clusters
        for i, label in enumerate(unique_labels):
            cluster = self.df[self.labels == label]
            distances = pdist(cluster)
            avg_distances[i] = distances.mean()
        return pd.Series(avg_distances, index=unique_labels)

    def intra_cluster_centroid_distance(self) -> pd.Series:
        """
        Returns the average distance between each data point in a cluster and the cluster centroid.
        """
        unique_labels = self.labels.unique()

        # Initialize a matrix to store the average distances
        avg_distances = np.zeros([len(unique_labels)])

        # Calculate the average distance between each pair of clusters
        for i, label in enumerate(unique_labels):
            cluster = self.df[self.labels == label]
            centroid = cluster.mean(axis=0)
            distances = cdist(cluster, centroid.values[np.newaxis, :])
            avg_distances[i] = distances.mean()
        return pd.Series(avg_distances, index=unique_labels)

    def difference_by_centroids(self, label1, label2):
        """
        Returns the difference between the centroids of two clusters.

        Adds information from the codebook.
        """
        means = self.get_centroids()
        dist = pd.DataFrame(
            {"dist": (means.loc[label1] - means.loc[label2]) ** 2, "label1": label1, "label2": label2}).join(
            self.ppdata.codebook)
        return dist.sort_values("dist", ascending=False)

    def difference_by_quantiles(self, label1, label2, q=0.25):
        """
        Returns the difference between the quantiles of two clusters.

        Adds information from the codebook.
        """
        res = dict()
        l1df = self.df[self.labels == label1]
        l2df = self.df[self.labels == label2]
        for feature in l1df.columns:
            if l1df[feature].quantile(.5) > l2df[feature].quantile(.5):
                diff = l1df[feature].quantile(.5 - q) - l2df[feature].quantile(.5 + q)
            else:
                diff = l2df[feature].quantile(.5 - q) - l1df[feature].quantile(.5 + q)
            res[feature] = (label1, label2, diff)
        dist = pd.DataFrame(res, index=["label1", "label2", "dist"]).T.join(self.ppdata.codebook)
        return dist.sort_values("dist", ascending=False)

    def difference_pairwise(self, label1, label2):
        """
        Returns the average pairwise difference between the given clusters for each feature.
        """
        res = dict()
        l1df = self.df[self.labels == label1]
        l2df = self.df[self.labels == label2]
        for feature in l1df.columns:
            diff = cdist(l1df[[feature]], l2df[[feature]]).mean()
            res[feature] = (label1, label2, diff)
        dist = pd.DataFrame(res, index=["label1", "label2", "dist"]).T.join(self.ppdata.codebook)
        return (dist.sort_values("dist", ascending=False))

    def plot_difference(self, difference_df):
        """
        Plots the density plot for the top 3 features with the highest difference between two clusters.
        """
        # Assuming 'optimal_dbscan' is your DBSCAN model and 'dist' is the DataFrame containing the feature differences
        top_3_features = difference_df.sort_values("dist", ascending=False).index[:3]

        for feature in top_3_features:
            plt.figure(figsize=(10, 6))
            for label in self.labels.unique():
                sns.kdeplot(self.df[self.labels == label][feature], label=f'Cluster {label}')
            question = self.ppdata.codebook.loc[feature, 'title']
            plt.title(f'Density Plot for {feature} ({question})')
            plt.xlabel('Value')
            plt.ylabel('Density')
            plt.legend()
            plt.show()


class DbscanAnalysis(ClusterAnalysis):
    """
    A class to perform DBSCAN cluster analysis on preprocessed data.
    """
    method = "DBSCAN"

    def __init__(self, ppdata, dbscan):
        """
        Initializes a DBSCAN cluster analysis object.
        Calculates the cluster labels.
        """
        self.dbscan = dbscan
        labels = pd.Series(dbscan.fit_predict(ppdata.df), index=ppdata.df.index)
        super().__init__(ppdata, labels)


class FCMAnalysis(ClusterAnalysis):
    """
    A class to perform Fuzzy C-Means cluster analysis on preprocessed data.
    """
    method = "Fuzzy C-Means"

    def __init__(self, ppdata, fcm: FCM):
        """
        Initializes a Fuzzy C-Means cluster analysis object.

        Calculates labels and soft labels.
        """
        self.fcm = fcm
        fcm.fit(ppdata.df.values)
        self.soft_labels = fcm.soft_predict(ppdata.df.values)
        self.__soft_sse = None
        labels = fcm.predict(ppdata.df.values)
        super().__init__(ppdata, pd.Series(labels, index=ppdata.df.index))

    def _calculate_soft_sse(self):
        """
        Calculates the sum of squared distances (SSE) between data points and their cluster centroids (mean),
        taking into account the degree of membership of each data point in each cluster (soft labels).
        """
        unique_labels = np.unique(self.labels)
        sse = 0

        for label in unique_labels:
            data_in_cluster = self.df[self.labels == label]
            centroid = data_in_cluster.mean(axis=0)
            distances = cdist(data_in_cluster, centroid.values[np.newaxis, :])
            weights = self.soft_labels[self.labels == label]
            sse += np.sum(weights * distances ** 2)

        return sse

    def get_soft_sse(self):
        """
        Returns the sum of squared distances, calculated using the soft labels.
        """
        if self.__soft_sse is None:
            self.__soft_sse = self._calculate_soft_sse()
        return self.__soft_sse


    def plot_scatterplots(self, n_scatterplots):
        """
        Plots scatterplots of the data projected onto two principal components per plot.

        Adds centroids.
        """
        # Check that the number of scatterplots is valid
        if n_scatterplots * 2 > len(self.df.columns):
            raise ValueError("n_scatterplots * 2 cannot be greater than the number of dimensions in the DataFrame")

        # Discrete color labels
        unique_labels = np.unique(self.labels)
        cmap = plt.get_cmap('viridis', len(unique_labels))
        colors = [cmap(i) for i in range(len(unique_labels))]
        color_map = dict(zip(unique_labels, colors))
        legend_handles = [mpatches.Patch(color=cmap(i), label=f'Cluster {label}') for i, label in
                          enumerate(unique_labels)]

        # Calculate the centroids for each cluster
        centroids_df = self.get_centroids()

        # Transform the centroids using PCA
        centroids_pca = pd.DataFrame(self.pca.transform(centroids_df), columns=centroids_df.columns,
                                     index=centroids_df.index)

        # Create scatterplots
        for i in range(n_scatterplots):
            plt.figure(figsize=(16, 12))
            plt.scatter(self.X_pca[:, i * 2], self.X_pca[:, i * 2 + 1], c=self.labels, marker='o', cmap='viridis',
                        edgecolor='k')
            # Plot the centroids
            for label, centroid in centroids_pca.iterrows():
                plt.scatter(centroid.iloc[i * 2], centroid.iloc[i * 2 + 1],
                            color=color_map[label], marker='s', s=50, edgecolor='red', linewidth=2)

            plt.title(f'DBSCAN Clustering Projection on PCA Components {i * 2 + 1} and {i * 2 + 2}')
            plt.xlabel(f'PCA Component {i * 2 + 1}')
            plt.ylabel(f'PCA Component {i * 2 + 2}')
            plt.legend(handles=legend_handles)
            plt.show()



class PreprocessedDataProvider:
    """
    A class to provide preprocessed data objects.
    """
    def __init__(self, sourceDf: pd.DataFrame, sourceCodebookDf: pd.DataFrame,
                 logger: logging.Logger):
        self.sourceDf = sourceDf.copy()
        self.sourceCodebookDf = sourceCodebookDf.copy()
        self.logger = logger
        self.__preprocDataCache = {}

    def get(
            self,
            educationOrdinalOption: OrdinalOption = OrdinalOption.KEEP_AS_NUMERIC,
            imputer: Optional[_BaseImputer] = None,
            transformer: Optional[TransformerMixin] = None,
            group_selector: Optional[Callable[[str], str]] = None,
    ) -> PreprocessedData:
        """
        Create a PreprocessedData object with the global variables df, codebookDf, and logger. Always drops remaining_na and enforces the codebook.
        """
        key = (
            educationOrdinalOption, str(imputer), str(transformer), group_selector.__name__ if group_selector else None)
        if key in self.__preprocDataCache:
            self.logger.debug("Cache hit!")
            return self.__preprocDataCache[key]
        self.logger.debug("Cache miss!")
        value = PreprocessedData(
            df=self.sourceDf,
            codebook=self.sourceCodebookDf,
            enforce_codebook=True,
            ordinalOptions={"F7h": educationOrdinalOption},
            imputer=imputer,
            transformer=transformer,
            drop_remaining_na=True,
            group_selector=group_selector,
            logger=self.logger
        )
        self.__preprocDataCache[key] = value
        return value


def prepare_dataframes(
        provider: PreprocessedDataProvider,
        ordinal_options: List[OrdinalOption],
        imp_opts: List[SimpleImputer],
        trans_opts: List[TransformerMixin],
        group_selectors: List[Optional[Callable]]
) -> DataFrame:
    """
    Prepare all possible dataframes/preparations.

    :param ordinal_options: List of ordinal options to be used for preprocessing.
    :param imp_opts: List of imputer options to be used for preprocessing.
    :param trans_opts: List of transformer options to be used for preprocessing.
    :param group_selectors: List of group selector options to be used for preprocessing.
    :return: A DataFrame containing all possible combinations of preprocessing steps.
    """
    all_processed_data = pd.DataFrame(
        columns=['ordinalOption', 'imputer_name', 'transformer_name', 'group_selector_name', 'data'])

    for ordinal_option in ordinal_options:
        for imputer_name, imputer in zip((str(o) for o in imp_opts), imp_opts):
            for transformer_name, transformer in zip((str(o) for o in trans_opts), trans_opts):
                for group_selector_name, group_selector in zip(["None", "groups", "groupsWithOthers"], group_selectors):
                    # Append the results to the DataFrame instead of the list
                    data = provider.get(ordinal_option, imputer, transformer, group_selector)
                    all_processed_data.loc[len(all_processed_data)] = {
                        'ordinalOption': ordinal_option,
                        'imputer_name': imputer_name,
                        'transformer_name': transformer_name,
                        'group_selector_name': group_selector_name,
                        'data': data
                    }

    if not (all_processed_data['data'].value_counts() == 1).all():
        raise ValueError("There are duplicates in the data.")

    return all_processed_data


def binary_search(fun, target, lower_bound, upper_bound, threshold):
    """
    Binary search for a target value in a monotonically increasing function.
    """
    # check that there is actually a solution in the given range
    flower = fun(lower_bound)
    fupper = fun(upper_bound)

    if not flower < target < fupper:
        return None
    elif flower == target:
        return lower_bound, lower_bound
    elif fupper == target:
        return upper_bound, upper_bound

    while upper_bound - lower_bound > threshold:
        mid = (upper_bound + lower_bound) / 2.0
        if fun(mid) <= target:
            # fun(mid) <= target <= fun(upper_bound)
            lower_bound = mid
        else:
            # fun(mid) > target >= fun(lower_bound)
            upper_bound = mid

    return (lower_bound, upper_bound)


def find_epsilon_lower(df, min_samples, max_outliers_portion=0.5):
    """
    Find the lower bound for the epsilon parameter in DBSCAN.
    """
    lower_bound = 1e-15
    # *2+10.0 for safety and float precision
    upper_bound = (sum((df.max() - df.min()) ** 2)) ** .5 * 2 + 10.0
    threshold = max(1e-10, (upper_bound - lower_bound) * 1e-7)
    size = len(df)

    def target_fun(epsilon):
        """
        Target function for binary search.
        Portion of the inliers (all - outliers) / all
        """
        dbscan = DBSCAN(eps=epsilon, min_samples=min_samples)
        dbscan_labels = dbscan.fit_predict(df)
        label_counts = pd.Series(dbscan_labels).value_counts()
        return (size - label_counts.get(-1, 0)) / size

    return binary_search(target_fun, max_outliers_portion, lower_bound, upper_bound, threshold)


def find_epsilon_upper(df, min_samples, biggest_cluster_portion=0.5):
    """
    Find the upper bound for the epsilon parameter in DBSCAN.
    """
    lower_bound = 1e-15
    # *2+10.0 for safety and float precision
    upper_bound = (sum((df.max() - df.min()) ** 2)) ** .5 * 2 + 10.0
    threshold = max(1e-10, (upper_bound - lower_bound) * 1e-7)
    size = len(df)

    def target_fun(epsilon):
        """
        Target function for binary search.
        Relative size of the biggest cluster (expect outliers, -1).
        """
        dbscan = DBSCAN(eps=epsilon, min_samples=min_samples)
        dbscan_labels = dbscan.fit_predict(df)
        label_counts = pd.Series(dbscan_labels).value_counts()
        target_val = label_counts.drop(-1, errors='ignore').max()
        target_val = target_val if not np.isnan(target_val) else 0
        return target_val / size

    return binary_search(target_fun, biggest_cluster_portion, lower_bound, upper_bound, threshold)


def find_epsilon_range(df, min_samples, min_outliers_portion=0.5, biggest_cluster_portion=0.5):
    """
    Find a range for optimal the epsilon parameter in DBSCAN, bounded by the given proportions of outliers (for lower
     bound) and the biggest cluster size (for upper bound).
     Usually, these numbers should be 0.5.
    """
    return (find_epsilon_lower(df, min_samples, min_outliers_portion)[0],
            find_epsilon_upper(df, min_samples, biggest_cluster_portion)[1])


def epsilon_fitness(value_counts):
    """
    A simple fitness function for evaluating DBSCAN clustering parameters, preferring multiple clusters.
    """
    return sum(value_counts ** 0.5)


def exhaustive_dbscan_clustering(preprocessed_data: pd.DataFrame, min_samples_options: list, num_eps_options: int,
                                 output_dir: str | None = None, logger: logging.Logger | None = None) -> None | pd.DataFrame:
    """
    Performs DBSCAN clustering with different parameters and saves the results to a CSV file.

    :param preprocessed_data: DataFrame containing all possible combinations of preprocessing steps.
    :param min_samples_options: List of min_samples values to use for DBSCAN.
    :param num_eps_options: Number of epsilon values to use for DBSCAN.
    :param output_dir: Directory where the output CSV files will be saved.

    :return: None if output_dir is not None, else a DataFrame containing the DBSCAN analysis results.
    """
    (not logger) or logger.info("Starting the DBSCAN clustering with different parameters.")
    for min_samples in min_samples_options:
        (not logger) or logger.info(f"Starting with min_samples={min_samples}.")
        results_df = pd.DataFrame(
            columns=["ordinalOption", "imputer_name", "transformer_name", "group_selector_name", "min_samples", "eps",
                     "fitness", "value_counts"])
        for i, ppdata in enumerate(preprocessed_data["data"]):
            (not logger) or logger.info(f"{str(i / len(preprocessed_data) * 100)} %")
            eps_range = find_epsilon_range(ppdata.df, min_samples)
            eps_options = np.linspace(eps_range[0], eps_range[1], num_eps_options)

            for eps in eps_options:
                dbscan_labels = DBSCAN(eps=eps, min_samples=min_samples).fit_predict(ppdata.df)
                value_counts = pd.Series(dbscan_labels).value_counts()
                fitness = epsilon_fitness(value_counts)

                results_df.loc[len(results_df)] = [preprocessed_data.iloc[i]["ordinalOption"],
                                                   preprocessed_data.iloc[i]["imputer_name"],
                                                   preprocessed_data.iloc[i]["transformer_name"],
                                                   preprocessed_data.iloc[i]["group_selector_name"],
                                                   min_samples, eps, fitness, value_counts.to_dict()]
        (not logger) or logger.info(f"Finished with min_samples={min_samples}.")

        if output_dir is not None:
            # write results_df to disk
            results_df.to_csv(os.path.join(output_dir, f"results_df_{min_samples}.csv"))
            return None
        else:
            return results_df

def calculate_fcm_scores(ppd: PreprocessedData, n_clusters_options: List[int], m: float,
                         random_state: int) -> pd.DataFrame:
    """
    Calculates Fuzzy C-Means clustering scores for a range of cluster numbers.

    :param ppd: The input data
    :param n_clusters_options: The different numbers of clusters to use for FCM.
    :param m: The fuzziness parameter for the FCM algorithm.
    :param random_state: The random state to use for the FCM algorithm.

    :return: A DataFrame containing the FCM analysis results for each number of clusters in the range.
    """
    results = []
    for n_clusters in n_clusters_options:
        fcm_analysis = FCMAnalysis(ppd, FCM(n_clusters=n_clusters, m=m, random_state=random_state))
        results.append({
            'm': m,
            'n_clusters': n_clusters,
            'FCMAnalysis': fcm_analysis,
            'sse': fcm_analysis.get_sse(),
            'soft_sse': fcm_analysis.get_soft_sse(),
            'dunn_index': fcm_analysis.get_dunn_index(),
            'silhouette_score': fcm_analysis.get_silhouette_score()
        })

    return pd.DataFrame(results)

def plot_cohesion(prepro: PreprocessedData, min_samples=50):
    """
    Plots the cohesion of the "inliers" for different epsilon values.
    Cohesion is the sum of the standard deviations of the features in the 0-cluster.
    """
    eps_range = find_epsilon_range(prepro.df, min_samples, min_outliers_portion=0.05, biggest_cluster_portion=0.95)
    cohesion_values = dict()
    for eps in np.linspace(eps_range[0], eps_range[1], 20):
        dbsa = DbscanAnalysis(prepro, DBSCAN(eps=eps, min_samples=min_samples))
        cohesion_values[eps] = dbsa.df[dbsa.labels == 0].std().sum()

    eps_values = list(cohesion_values.keys())
    cohesion_values_list = list(cohesion_values.values())

    plt.figure(figsize=(10, 6))
    plt.plot(eps_values, cohesion_values_list, marker='o')
    plt.title('Cohesion values for different eps values')
    plt.xlabel('eps')
    plt.ylabel('Cohesion value')
    plt.grid(True)
    return plt


def plot_answer_change_scatterplot(t1_data, t1_labels, t2_data, t2_labels, columns, codebookDf, show_plots = True):
    """
    Plots the change in answers between time period t_1 and t_2.
    x-coordinate is t_1, y-coordinate is t_2.

    If show_plots is false, the plots are returned as a list.

    Four subplots are created for each pair of label in t_1 and label in t_2.
    """
    def get_label(label_t1, label_t2):
        """
        Creates a label in the form t_1 -> t_2.
        """
        if label_t1 == 0 and label_t2 == 0:
            return '0 -> 0'
        elif label_t1 == -1 and label_t2 == -1:
            return '-1 -> -1'
        elif label_t1 == 0 and label_t2 == -1:
            return '0 -> -1'
        elif label_t1 == -1 and label_t2 == 0:
            return '-1 -> 0'

    all_plots = []

    # Iterate over each column in the DataFrame
    for column in columns:
        # Create a new DataFrame for plotting
        plot_df = pd.DataFrame({
            't1': t1_data.loc[:, column],
            't2': t2_data.loc[:, column],
            'color': [get_label(t1_labels.iloc[i], t2_labels.iloc[i]) for i in range(len(t1_labels))],
        })

        # Create a scatter plot with dodge and alpha
        plot = ggplot(plot_df, aes(x='t1', y='t2', color='color')) + \
               geom_point(alpha=0.1, position=position_jitter(width=0.5, height=0.5)) + \
               geom_abline(slope=1, intercept=0, color='gray', linetype='dashed') + \
               ggtitle(f"Change of answers for {column} ({codebookDf.loc[column, 'title']})") + \
               xlab('Answer t_1') + \
               ylab('Answer t_2') + \
               theme(legend_position='none') + \
               facet_wrap('color')
        if show_plots:
            plot.show()
        else:
            all_plots.append(plot)

    if not show_plots:
        return all_plots
    else:
        return None

def create_sankey_diagram_tchange(t1_data: pd.DataFrame, t1_labels, t2_data: pd.DataFrame, t2_labels) -> go.Figure:
    df_t1 = t1_data.copy()
    df_t2 = t2_data.copy()

    df_t1['label_t1'] = t1_labels
    df_t2['label_t2'] = t2_labels

    merged_df = pd.merge(df_t1, df_t2, on='unique_id', suffixes=('_t1', '_t2'))

    clusters_of_interest = [-1, 0]

    source = []
    target = []
    value = []

    cluster_mapping = {
        't1_-1': 0,  # t1 Cluster -1
        't1_0': 1,  # t1 Cluster 0
        't2_-1': 2,  # t2 Cluster -1
        't2_0': 3  # t2 Cluster 0
    }
    label = ["t1 Cluster -1", "t1 Cluster 0", "t2 Cluster -1", "t2 Cluster 0"]

    # Count transitions from t1 clusters to t2 clusters
    for cluster_t1 in clusters_of_interest:
        for cluster_t2 in clusters_of_interest:
            count = merged_df[(merged_df['label_t1'] == cluster_t1) & (merged_df['label_t2'] == cluster_t2)].shape[0]
            if count > 0:
                source.append(cluster_mapping[f't1_{cluster_t1}'])
                target.append(cluster_mapping[f't2_{cluster_t2}'])
                value.append(count)

    fig = go.Figure(data=[go.Sankey(
        node=dict(
            pad=15,
            thickness=20,
            line=dict(color="black", width=0.5),
            label=label,
            color="blue"
        ),
        link=dict(
            source=source,
            target=target,
            value=value
        )
    )])

    fig.update_layout(title_text="Cluster Transitions from t1 to t2", font_size=10)

    num_t1_minus_1 = merged_df[merged_df['label_t1'] == -1].shape[0]
    num_t1_0 = merged_df[merged_df['label_t1'] == 0].shape[0]
    num_t2_minus_1 = merged_df[merged_df['label_t2'] == -1].shape[0]
    num_t2_0 = merged_df[merged_df['label_t2'] == 0].shape[0]

    num_t1_minus_1_to_t2_0 = merged_df[(merged_df['label_t1'] == -1) & (merged_df['label_t2'] == 0)].shape[0]
    num_t1_0_to_t2_minus_1 = merged_df[(merged_df['label_t1'] == 0) & (merged_df['label_t2'] == -1)].shape[0]

    print(f"Number of people in T1 in cluster -1: {num_t1_minus_1}")
    print(f"Number of people in T1 in cluster 0: {num_t1_0}")
    print(f"Number of people in T2 in cluster -1: {num_t2_minus_1}")
    print(f"Number of people in T2 in cluster 0: {num_t2_0}")

    print(f"Number of people changing from -1 to 0: {num_t1_minus_1_to_t2_0}")
    print(f"Number of people changing from 0 to -1: {num_t1_0_to_t2_minus_1}")

    return fig
